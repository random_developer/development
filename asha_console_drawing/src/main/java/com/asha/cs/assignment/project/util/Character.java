package com.asha.cs.assignment.project.util;

public class Character {
    public static final char HORIZONTAL_EDGE_CHAR = '-';
    public static final char VERTICAL_EDGE_CHAR   = '|';
    public static final char LINE_CHAR            = 'x';
}
