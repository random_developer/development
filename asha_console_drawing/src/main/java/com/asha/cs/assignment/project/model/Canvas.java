

package com.asha.cs.assignment.project.model;

import com.asha.cs.assignment.project.entity.Entity;
import com.asha.cs.assignment.project.entity.InvalidEntityException;

public interface Canvas {

    void addEntity(Entity entity) throws InvalidEntityException;

    String render();
}
