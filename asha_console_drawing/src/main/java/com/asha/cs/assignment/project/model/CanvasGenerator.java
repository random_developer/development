package com.asha.cs.assignment.project.model;

import com.asha.cs.assignment.project.entity.Entity;
import com.asha.cs.assignment.project.entity.InvalidEntityException;
import com.asha.cs.assignment.project.shape.BucketFill;
import com.asha.cs.assignment.project.shape.Line;
import com.asha.cs.assignment.project.shape.Point;
import com.asha.cs.assignment.project.shape.Rectangle;
import com.asha.cs.assignment.project.util.Character;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CanvasGenerator implements Canvas {
    private Character chr = new Character();
    private final char[][]  canvasPlotArray;
    private final int width;
    private final int height;
  //  private LinkedList<Entity> entities;
    private final String horizontalEdge;

    public CanvasGenerator(int w, int h) {
        width = w;
        height = h;
        //entities = new LinkedList<>();
        canvasPlotArray = new char[this.height][this.width];
        //Arrays.stream(canvasPlotArray).forEach(chars -> Arrays.fill(chars, ' '));
        for(char[] row : canvasPlotArray)
            Arrays.fill(row,' ' );
        horizontalEdge = Stream.generate(() -> String.valueOf(chr.HORIZONTAL_EDGE_CHAR)).limit(width + 2).collect(Collectors.joining());
    }

    @Override
    public void addEntity(Entity entity) throws InvalidEntityException {
       // entities.add(entity);
        if (entity instanceof Line) {
            addLine((Line) entity);
        }else if (entity instanceof Rectangle) {
            addRectangle((Rectangle) entity);
        }else if(entity instanceof BucketFill){
            addBucketFill((BucketFill) entity);
        }
    }


    @Override
    public String render() {
        StringBuilder builder = new StringBuilder();
        builder.append(horizontalEdge).append("\n");
        for (int i = 0; i < this.height; i++) {
            builder.append(chr.VERTICAL_EDGE_CHAR);
            for (int j = 0; j < this.width; j++) {
                builder.append(canvasPlotArray[i][j]);
            }
            builder.append(chr.VERTICAL_EDGE_CHAR);
            builder.append("\n");
        }
        builder.append(horizontalEdge);
        return builder.toString();
    }
    private void addLine(Line line) {
        if (isOutside(line.getX1(), line.getY1())) {
            throw new InvalidEntityException("Line is outside of canvas");
        }

        //trim the part the is outside
        if (line.getX2() >= width) {
            line.setX2(width);
        }
        if (line.getY2() >= height) {
            line.setY2(height);
        }
        drawLine(line.getX1(), line.getY1(), line.getX2(), line.getY2());
    }
    private void drawLine(int x1, int y1, int x2, int y2) {
        //row by row
        for (int row = y1 - 1; row <= y2 - 1 && row < height; row++) {
            //col by col
            for (int col = x1 - 1; col <= x2 - 1 && col < width; col++) {
                canvasPlotArray[row][col] = chr.LINE_CHAR;
            }
        }
    }
    private void addRectangle(Rectangle rec) {
        if (isOutside(rec.getX1(), rec.getY1())) {
            throw new InvalidEntityException("Rectangle is outside of canvas");
        }
        drawRectangle(rec.getX1(), rec.getY1(), rec.getX2(), rec.getY2());
    }
    private void drawRectangle(int x1, int y1, int x2, int y2) {
        //top edge
        drawLine(x1, y1, x2, y1);
        //right edge
        drawLine(x1, y1, x1, y2);
        //bottom edge
        drawLine(x2, y1, x2, y2);
        //right edge
        drawLine(x1, y2, x2, y2);
    }
    private void addBucketFill(BucketFill bucketFill) {
        if (isOutside(bucketFill.getX(), bucketFill.getY())) {
            throw new InvalidEntityException("Bucket fill point is outside of canvas");
        }
        fillBucket(bucketFill.getX(), bucketFill.getY(), bucketFill.getCharacter());
    }
    private void fillBucket(int x, int y, char mchar) {
        char originalChar = canvasPlotArray[y - 1][x - 1];
        Stack<Point> stack        = new Stack<>();
        stack.add(new Point(y - 1, x - 1));
        while (!stack.isEmpty()) {
            Point pop = stack.pop();
            if (canvasPlotArray[pop.getX()][pop.getY()] == originalChar) {
                canvasPlotArray[pop.getX()][pop.getY()] = mchar;
            }
            if (pop.getX() - 1 >= 0 && canvasPlotArray[pop.getX() - 1][pop.getY()] == originalChar) {
                stack.add(new Point(pop.getX() - 1, pop.getY()));
            }
            if (pop.getX() + 1 < height && canvasPlotArray[pop.getX() + 1][pop.getY()] == originalChar) {
                stack.add(new Point(pop.getX() + 1, pop.getY()));
            }
            if (pop.getY() - 1 >= 0 && canvasPlotArray[pop.getX()][pop.getY() - 1] == originalChar) {
                stack.add(new Point(pop.getX(), pop.getY() - 1));
            }
            if (pop.getY() + 1 < width && canvasPlotArray[pop.getX()][pop.getY() + 1] == originalChar) {
                stack.add(new Point(pop.getX(), pop.getY() + 1));
            }
        }
    }
    private boolean isOutside(int x, int y) {
        return x < 1 || x >= width || y < 1 || y >= height;
    }
}
