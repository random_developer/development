package com.asha.cs.assignment.project.functionality;

import com.asha.cs.assignment.project.entity.BucketFillCommand;
import com.asha.cs.assignment.project.entity.LineCommand;
import com.asha.cs.assignment.project.entity.RectangleCommand;

import java.util.Arrays;

public class CommandFactory implements Command{
    public Command getCommand(String commandLine){
        commandLine = commandLine.trim().replaceAll(" {2}", " ");
        String[] split       = commandLine.split(" ");
        String   mainCommand = split[0].toUpperCase();
        String[] params      = Arrays.copyOfRange(split, 1, split.length);
        if(mainCommand.equals("Q") ){
            return new QuitCommand();
        }else if(mainCommand.equals("C")){
            return new CreateCommand(params);
        }else if(mainCommand.equals("L")){
            return new LineCommand(params);
        }else if(mainCommand.equals("R")){
            return new RectangleCommand(params);
        }else if( mainCommand.equals("B")){
            return new BucketFillCommand(params);
        }
        else{
            throw new InvalidInputException();
        }


    }
}
