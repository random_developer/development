package com.asha.cs.assignment.project.entity;

import com.asha.cs.assignment.project.functionality.Command;

public interface EntityCommand extends Command {
}
