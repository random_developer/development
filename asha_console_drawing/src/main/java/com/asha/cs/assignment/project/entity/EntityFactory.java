
package com.asha.cs.assignment.project.entity;


import com.asha.cs.assignment.project.shape.BucketFill;
import com.asha.cs.assignment.project.shape.Line;
import com.asha.cs.assignment.project.shape.Rectangle;

public class EntityFactory {

    public Entity getEntity(EntityCommand command) {
        if (command instanceof LineCommand) {
            LineCommand cmd = (LineCommand) command;
            return new Line(cmd.getX1(), cmd.getY1(), cmd.getX2(), cmd.getY2());
        }
        if(command instanceof RectangleCommand){
            RectangleCommand cmd = (RectangleCommand) command;
            return  new Rectangle(cmd.getX1(), cmd.getY1(), cmd.getX2(), cmd.getY2());
        }else if (command instanceof BucketFillCommand) {
            BucketFillCommand cmd = (BucketFillCommand) command;
            return new BucketFill(cmd.getX(), cmd.getY(), cmd.getCharacter());
        }
        return null;
    }
}
