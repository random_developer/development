package com.asha.cs.assignment.project.entity;

import com.asha.cs.assignment.project.functionality.InvalidInputParams;
import com.asha.cs.assignment.project.util.Message;
import com.asha.cs.assignment.project.util.Utilities;

public class LineCommand implements EntityCommand{
    private Message msg  = new Message();
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    public LineCommand(String... params) {
        if (params.length < 4)
            throw new InvalidInputParams("Draw line command expects 4 params", msg.lineMessage);
        try {
            x1 = Utilities.parsetoPositiveInt(params[0]);
            y1 = Utilities.parsetoPositiveInt(params[1]);
            x2 = Utilities.parsetoPositiveInt(params[2]);
            y2 = Utilities.parsetoPositiveInt(params[3]);
        } catch (IllegalArgumentException e) {
            throw new InvalidInputParams("Number should be > 0", msg.lineMessage);
        }
        if (x1 != x2 && y1 != y2) {
            throw new InvalidInputParams("Draw line does not support diagonal line at the moment", msg.lineMessage);
        }
    }
    public int getX1() {
        return x1;
    }

    public LineCommand setX1(int x1) {
        this.x1 = x1;
        return this;
    }

    public int getY1() {
        return y1;
    }

    public LineCommand setY1(int y1) {
        this.y1 = y1;
        return this;
    }

    public int getX2() {
        return x2;
    }

    public LineCommand setX2(int x2) {
        this.x2 = x2;
        return this;
    }

    public int getY2() {
        return y2;
    }

    public LineCommand setY2(int y2) {
        this.y2 = y2;
        return this;
    }


}
