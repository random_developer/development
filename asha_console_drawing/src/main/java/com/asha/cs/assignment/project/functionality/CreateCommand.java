package com.asha.cs.assignment.project.functionality;

import com.asha.cs.assignment.project.util.Message;
import com.asha.cs.assignment.project.util.Utilities;

public class CreateCommand implements Command {
    private Message msg  = new Message();
    private int height;
    private int width;

    public CreateCommand(String... params) {
        if (params.length < 2)
            throw new InvalidInputParams("Create command require 2 parameters", msg.createMessage);
        try {
            width = Utilities.parsetoPositiveInt(params[0]);
            height = Utilities.parsetoPositiveInt(params[1]);
        } catch (IllegalArgumentException e) {
            throw new InvalidInputParams("Number must >= 0",  msg.createMessage);
        }
    }

    public int getHeight() {
        return height;
    }

    public CreateCommand setHeight(int height) {
        this.height = height;
        return this;
    }

    public int getWidth() {
        return width;
    }

    public CreateCommand setWidth(int width) {
        this.width = width;
        return this;
    }
}