

package com.asha.cs.assignment.project.functionality;

public class InvalidInputParams extends RuntimeException {

    private final String help;

    public InvalidInputParams(String message, String _help) {
        super(message);
        help = _help;
    }

    public String getHelpMessage() {
        return help;
    }
}