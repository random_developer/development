package com.asha.cs.assignment.project.service;

import com.asha.cs.assignment.project.entity.EntityCommand;
import com.asha.cs.assignment.project.entity.EntityFactory;
import com.asha.cs.assignment.project.entity.InvalidEntityException;
import com.asha.cs.assignment.project.functionality.*;
import com.asha.cs.assignment.project.model.Canvas;
import com.asha.cs.assignment.project.model.CanvasGenerator;

import java.util.Scanner;

public class CommandRunner {
    private static Scanner scanner;
    private static Canvas canvas;
    private static CommandFactory commandFactory;
    private static EntityFactory entityFactory;
    public static void commandRunner(){
        printHelp();
        scanner = new Scanner(System.in);
        commandFactory = new CommandFactory();
        entityFactory = new EntityFactory();
        System.out.println("Enter command:");
        while (true) {
            processInput(scanner.nextLine());
            System.out.println("Enter command:");
        }

    }
    private static void processInput(String commandLine) {
        Command command = null;
        try {
            System.out.println(commandLine);
            command = commandFactory.getCommand(commandLine);
        } catch (InvalidInputException e) {
            System.out.println("Please enter a valid command.");
        } catch (InvalidInputParams invalidInputParams) {
            System.out.println("Command syntax is not correct: " + invalidInputParams.getMessage());
            System.out.println("Refer to following correct syntax: \n" + invalidInputParams.getHelpMessage());
        }
        if (command instanceof QuitCommand) {
            scanner.close();
            System.out.println("Exiting...");
            System.exit(0);
        }
        if (command instanceof CreateCommand) {
            createNewCanvas((CreateCommand) command);
            return;
        }
        if (command instanceof EntityCommand) {
            draw((EntityCommand) command);
        }

    }
    private static void createNewCanvas(CreateCommand command) {
        canvas = new CanvasGenerator(command.getWidth(), command.getHeight());
        System.out.println(canvas.render());
    }

    private static void draw(EntityCommand command) {
        if (canvas == null) {
            System.out.println("You need to create a canvas first");
            return;
        }
        try {
            canvas.addEntity(entityFactory.getEntity(command));
            System.out.println(canvas.render());
        } catch (InvalidEntityException e) {
            System.out.println("Can not add the model to canvas: " + e.getMessage());
        }
    }

    private static void printHelp() {
        String help = "The work as follows:\n"
                + "1. Create a new canvas \n"
                + "2. Start Drawing on the canvas by issuing various commmands \n"
                + "3. Quit \n\n\n"
                + "|Command 		|Description|\n"
                + "|----|----|\n"
                + "|C w h          | Should create a new canvas of width w and height h.|\n"
                + "|L x1 y1 x2 y2  | Should create a new line from (x1,y1) to (x2,y2). Currently, only|\n"
                + "|               | horizontal or vertical lines are supported. Horizontal and vertical lines|\n"
                + "|               | will be drawn using the 'x' character.|\n"
                + "|R x1 y1 x2 y2  | Should create a rectangle whose upper left corner is (x1,y1) and|\n"
                + "|               | lower right corner is (x2,y2). Horizontal and vertical lines will be drawn|\n"
                + "|               | using the 'x' character.|\n"
                + "|B x y c        | Fill the entire area connected to (x,y) with \"colour\" c. The|\n"
                + "|               | behaviour of this is the same as that of the \"bucket fill\" tool in paint|\n"
                + "|               | programs.|\n"
                + "|Q              | Quit|\n";
        System.out.println(help);
    }


}
