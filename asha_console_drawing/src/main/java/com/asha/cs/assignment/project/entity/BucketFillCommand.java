
package com.asha.cs.assignment.project.entity;
import com.asha.cs.assignment.project.functionality.InvalidInputParams;
import com.asha.cs.assignment.project.util.Message;
import com.asha.cs.assignment.project.util.Utilities;

public class BucketFillCommand implements EntityCommand {
    private Message msg = new Message();
    private int  x;
    private int  y;
    private char character;

    public BucketFillCommand(String... params) {
        if (params.length < 3)
            throw new InvalidInputParams("Bucket fill expects 3 params", msg.bucketMessage);
        if (params[2].length() != 1)
            throw new InvalidInputParams("Color character should only be 1 character", msg.bucketMessage);
        try {
            x = Utilities.parsetoPositiveInt(params[0]);
            y = Utilities.parsetoPositiveInt(params[1]);
            character = params[2].charAt(0);
        } catch (IllegalArgumentException e) {
            throw new InvalidInputParams("x or y should be > 0", msg.bucketMessage);
        }
    }

    public int getX() {
        return x;
    }

    public BucketFillCommand setX(int x) {
        this.x = x;
        return this;
    }

    public int getY() {
        return y;
    }

    public BucketFillCommand setY(int y) {
        this.y = y;
        return this;
    }

    public char getCharacter() {
        return character;
    }

    public BucketFillCommand setCharacter(char character) {
        this.character = character;
        return this;
    }
}