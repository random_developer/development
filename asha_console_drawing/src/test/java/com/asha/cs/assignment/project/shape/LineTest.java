package com.asha.cs.assignment.project.shape;

import org.junit.Assert;
import org.junit.Test;

public class LineTest {

    @Test
    public void create() throws Exception {
        new Line(2, 3, 2, 4);
    }

    @Test
    public void create2() throws Exception {
        Line line1 = new Line(3, 1, 3, 2);
        Line line2 = new Line(3, 1, 3, 2);
        Assert.assertEquals(line1, line2);
    }
}
